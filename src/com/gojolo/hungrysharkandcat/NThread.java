package com.gojolo.hungrysharkandcat;

import android.graphics.Canvas;
import android.view.View;

import com.gojolo.hungrysharkandcat.Game.Gface;

public class NThread extends Thread {
	
	Gface myView;
	private boolean running = false;

	public NThread(Gface gface) {
		myView = gface;
	}
	
	public void setRunning(boolean run) {
        running = run;    
	}

	@Override
	public void run() {
				while(running){
					
					final Canvas canvas = myView.getHolder().lockCanvas();
					
					if(canvas != null){
						synchronized (myView.getHolder()) {
							
							myView.draw(canvas);
							myView.getHolder().unlockCanvasAndPost(canvas);
						}

					}
					
					try {
						sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
		
	}

}
