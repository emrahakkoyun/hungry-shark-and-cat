package com.gojolo.hungrysharkandcat;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
public class Game extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(new Gface(this));
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	    
	}
public class Gface extends SurfaceView{
    NThread mthread;
    private SurfaceHolder sface;
    Bitmap olta,ekran,kbalik;
    Bitmap balik1,balik2,balik3,balik4,balik5,balik6,balik7,balik8,balik9,balik10;
    float oltay;
    int dokundu=0;
    int screenHeight,screenWidth;
    Paint white;
    int i=0,ii=0,b1=0,bb1=0,b2=0,bb2=0,b3=0,bb3=0,b4=0,bb4=0,b5=0,bb5=0,b6=0,bb6=0,b7=0,bb7=0,b8=0,bb8=0,b9=0,bb9=0,b10=0,bb10=0;
    int k=1,l=1,bl1=1,bk1=1,bl2=1,bk2=1,bl3=1,bk3=1,bl4=1,bk4=1,bl5=1,bk5=1,bl6=1,bk6=1,bl7=1,bk7=1,bl8=1,bk8=1,bl9=1,bk9=1,bl10=1,bk10=1;
    float kr,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,rk;
    int t1=0,t2=0,t3=0,t4=0,t5=0,t6=0,t7=0,t8=0,t9=0,t10=0,tk,intt=0;
    int tuttu=0;
    Intent intent;
    boolean mause=true;
	public Gface(Context context) {
		super(context);
		init();
	}
	private void init()
	{
        tuttu=0;
		intent  = new Intent(Game.this,Son.class);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		mthread = new NThread(this);
		sface = getHolder();
		olta =BitmapFactory.decodeResource(getResources(), R.drawable.olta);
		kbalik =BitmapFactory.decodeResource(getResources(),R.drawable.kbalikleft);
		kbalik = Bitmap.createScaledBitmap(kbalik,(int)(screenWidth/3.40),(int)(screenHeight/4.50), true);
		balik1 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik1 = Bitmap.createScaledBitmap(balik1,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik2 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik2 = Bitmap.createScaledBitmap(balik2,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik3 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik3 = Bitmap.createScaledBitmap(balik3,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik4 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik4 = Bitmap.createScaledBitmap(balik4,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik5 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik5 = Bitmap.createScaledBitmap(balik5,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik6 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik6 = Bitmap.createScaledBitmap(balik6,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik7 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik7 = Bitmap.createScaledBitmap(balik7,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik8 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik8 = Bitmap.createScaledBitmap(balik8,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik9 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik9 = Bitmap.createScaledBitmap(balik9,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		balik10 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		balik10 = Bitmap.createScaledBitmap(balik10,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
		i=(int)(screenWidth/3.40);
		b1=(int)(screenWidth/9.40);
		b2=(int)(screenWidth/9.40);
		b3=(int)(screenWidth/9.40);
		b4=(int)(screenWidth/9.40);
		b5=(int)(screenWidth/9.40);
		b6=(int)(screenWidth/9.40);
		b7=(int)(screenWidth/9.40);
		b8=(int)(screenWidth/9.40);
		b9=(int)(screenWidth/9.40);
		b10=(int)(screenWidth/9.40);
		sface.addCallback(new SurfaceHolder.Callback() {
			
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				boolean retry = true;
				mthread.setRunning(false);
                while (retry) {
                       try {
                    	   mthread.join();
                             retry = false;
                       } catch (InterruptedException e) {
                       }
                }
			}
			
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
		        Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.game);
		        ekran = Bitmap.createScaledBitmap(background, screenWidth, screenHeight, true);
				mthread.setRunning(true);
				mthread.start();
				
			}
			
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width,
					int height) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
 public void draw(Canvas canvas)
 { 
	 canvas.drawBitmap(ekran, 0, 0, null);
	 if(dokundu==1)
	 { 
	 if(oltay>(screenHeight/1.20))
	 {
		 oltay=(int)(screenHeight/1.20);
	 }
	 else{
		 if(mause==true)
	  oltay+=15;
	 }
	 }
	 if(dokundu==0)
	 { 
	 if(oltay<(screenHeight/9.86))
	 {	 oltay=(int)(screenHeight/9.86);
	 }
	 else{
		 if(mause==true)
	 oltay-=15;
	 }
	 }

	 Paint myPaint = new Paint();
	 myPaint.setColor(Color.parseColor("#c1c1ad"));
	 myPaint.setStrokeWidth(10);
	 canvas.drawRect((float)(screenWidth/1.338),(float)(screenHeight/9.86)+oltay, (float)(screenWidth/1.328), (float)(screenHeight/9.86), myPaint);
	 canvas.drawBitmap(olta,(float)(screenWidth/1.39),oltay, null);
     canvas.drawBitmap(kbalik,(float)(screenWidth/1.23)+i,(float)(screenHeight/kr), null);
     canvas.drawBitmap(balik1,(float)(screenWidth/5.40)+b1,(float)(screenHeight/r1), null);
      canvas.drawBitmap(balik2,(float)(screenWidth/10.40)+b2,(float)(screenHeight/r2), null);
      canvas.drawBitmap(balik3,(float)(screenWidth/4.40)+b3,(float)(screenHeight/r3), null);
      canvas.drawBitmap(balik4,(float)(screenWidth/15.40)+b4,(float)(screenHeight/r4), null);
      canvas.drawBitmap(balik5,(float)(screenWidth/8.40)+b5,(float)(screenHeight/r5), null);
     canvas.drawBitmap(balik6,(float)(screenWidth/9.40)+b6,(float)(screenHeight/r6), null);
      canvas.drawBitmap(balik7,(float)(screenWidth/3.40)+b7,(float)(screenHeight/r7), null);
      canvas.drawBitmap(balik8,(float)(screenWidth/13.40)+b8,(float)(screenHeight/r8), null);
     canvas.drawBitmap(balik9,(float)(screenWidth/19.40)+b9,(float)(screenHeight/r9), null);
     canvas.drawBitmap(balik10,(float)(screenWidth/21.40)+b10,(float)(screenHeight/r10), null);
     if(i<-(float)(screenWidth/8.44)&&i>-(float)(screenWidth/7.50)&&(kr>(float)(screenHeight/oltay)-1.0)&&(kr<(float)(screenHeight/oltay)+1.0))
	 {
		mause=false;
		myPaint.setAlpha(0);
	 }
     if(i>-(screenWidth+(int)(screenWidth/5))&&ii==0)
	 { 
		if(k==1)
		{kopekbalik(1);
		 Random rd = new Random();
		 kr = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		i-=30;
		k++;
		l=1;
	 }
	 else{
		 k=1;
		 if(l==1)
		 { kopekbalik(2);
		 Random rd = new Random();
		 kr = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 i+=30;
		 ii=1;
		 if(i>(screenWidth/5))
		 {
			 ii=0;
		 }
		 l++;
	 }
	 if(b1+(int)(screenWidth/3.40)>(float)(screenWidth/1.33)&&b1+(int)(screenWidth/3.40)<(float)(screenWidth/1.30)&&((r1>(float)(screenHeight/oltay)-1.0)&&(r1<(float)(screenHeight/oltay)+1.0)))
	 {
		t1=1;
		r1=(float)(screenHeight/oltay);
		if(r1>(float)(screenHeight/120))
		 {
			balik1.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t1=0;
		 }
	 }
	 else{ 
		 if(t1==0)
		 { if(b1>-(screenWidth)&&bb1==0)
	 { 
		if(bk1==1)
		{balik1(1);
		 Random rd = new Random();
		 r1 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b1-=10;
		bk1++;
		bl1=1;
	 }
	 else{
		 bk1=1;
		 if(bl1==1)
		 { balik1(2);
		 Random rd = new Random();
		 r1 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b1+=10;
		 bb1=1;
		 if(b1>((screenWidth)))
		 {
			 bb1=0;
		 }
		 bl1++;
	 }
	 }
	 }
	 if(b2+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b2+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r2>(float)(screenHeight/oltay)-1.0)&&(r2<(float)(screenHeight/oltay)+1.0)))
	 {
		t2=1;
		r2=(float)(screenHeight/oltay);
		if(r2>(float)(screenHeight/120))
		 {
			balik2.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t2=0;
		 }
	 }
	 else{ 
		 if(t2==0)
		 { 
	 if(b2>-(screenWidth)&&bb2==0)
	 { 
		if(bk2==1)
		{balik2(1);
		 Random rd = new Random();
		 r2 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b2-=20;
		bk2++;
		bl2=1;
	 }
	 else{
		 bk2=1;
		 if(bl2==1)
		 {balik2(2);
		 Random rd = new Random();
		 r2 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b2+=20;
		 bb2=1;
		 if(b2>((screenWidth)))
		 {
			 bb2=0;
		 }
		 bl2++;
	 }
		 }
	 }
	 if(b3+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b3+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r3>(float)(screenHeight/oltay)-1.0)&&(r3<(float)(screenHeight/oltay)+1.0)))
	 {
		t3=1;
		r3=(float)(screenHeight/oltay);
		if(r3>(float)(screenHeight/120))
		 {
			balik3.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t3=0;
		 }
	 }
	 else{ 
		 if(t3==0)
		 { 
	 if(b3>-(screenWidth)&&bb3==0)
	 { 
		if(bk3==1)
		{balik3(1);
		Random rd = new Random();
		 r3 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b3-=20;
		bk3++;
		bl3=1;
	 }
	 else{
		 bk3=1;
		 if(bl3==1)
		 {balik3(2);
		 Random rd = new Random();
		 r3 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b3+=20;
		 bb3=1;
		 if(b3>((screenWidth)))
		 {
			 bb3=0;
		 }
		 bl3++;
	 }
		 }}
	 if(b4+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b4+(int)(screenWidth/3.40)<(float)(screenWidth/1.18)&&((r4>(float)(screenHeight/oltay)-1.0)&&(r4<(float)(screenHeight/oltay)+1.0)))
	 {
		t4=1;
		r4=(float)(screenHeight/oltay);
		if(r4>(float)(screenHeight/120))
		 {
			balik4.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t4=0;
		 }
	 }
	 else{ 
		 if(t4==0)
		 { 
	 if(b4>-(screenWidth)&&bb4==0)
	 { 
		if(bk4==1)
		{balik4(1);
		Random rd = new Random();
		 r4 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b4-=25;
		bk4++;
		bl4=1;
	 }
	 else{
		 bk4=1;
		 if(bl4==1)
		 {balik4(2);
		 Random rd = new Random();
		 r4 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b4+=25;
		 bb4=1;
		 if(b4>((screenWidth)))
		 {
			 bb4=0;
		 }
		 bl4++;
	 }
		 }}
	 if(b5+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b5+(int)(screenWidth/3.40)<(float)(screenWidth/1.18)&&((r5>(float)(screenHeight/oltay)-1.0)&&(r5<(float)(screenHeight/oltay)+1.0)))
	 {
		t5=1;
		r5=(float)(screenHeight/oltay);
		if(r5>(float)(screenHeight/120))
		 {
			balik5.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t5=0;
		 }
	 }
	 else{ 
		 if(t5==0)
		 { 
	 if(b5>-(screenWidth)&&bb5==0)
	 { 
		if(bk5==1)
		{balik5(1);
		Random rd = new Random();
		 r5 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b5-=30;
		bk5++;
		bl5=1;
	 }
	 else{
		 bk5=1;
		 if(bl5==1)
		 {balik5(2);
		 Random rd = new Random();
		 r5 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b5+=30;
		 bb5=1;
		 if(b5>((screenWidth)))
		 {
			 bb5=0;
		 }
		 bl5++;
	 }}}
	 if(b6+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b6+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r6>(float)(screenHeight/oltay)-1.0)&&(r6<(float)(screenHeight/oltay)+1.0)))
	 {
		t6=1;
		r6=(float)(screenHeight/oltay);
		if(r6>(float)(screenHeight/120))
		 {
			balik6.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t6=0;
		 }
	 }
	 else{ 
		 if(t6==0)
		 { 
	 if(b6>-(screenWidth)&&bb6==0)
	 { 
		if(bk6==1)
		{balik6(1);
		Random rd = new Random();
		 r6 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b6-=20;
		bk6++;
		bl6=1;
	 }
	 else{
		 bk6=1;
		 if(bl6==1)
		 {balik6(2);
		 Random rd = new Random();
		 r6 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b6+=20;
		 bb6=1;
		 if(b6>((screenWidth)))
		 {
			 bb6=0;
		 }
		 bl6++;
	 }
		 }}
	 if(b7+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b7+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r7>(float)(screenHeight/oltay)-1.0)&&(r7<(float)(screenHeight/oltay)+1.0)))
	 {
		t7=1;
		r7=(float)(screenHeight/oltay);
		if(r7>(float)(screenHeight/120))
		 {
			balik7.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t7=0;
		 }
	 }
	 else{ 
		 if(t7==0)
		 { 
	 if(b7>-(screenWidth)&&bb7==0)
	 { 
		if(bk7==1)
		{balik7(1);
		Random rd = new Random();
		 r7 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b7-=50;
		bk7++;
		bl7=1;
	 }
	 else{
		 bk7=1;
		 if(bl7==1)
		 { balik7(2);
		 Random rd = new Random();
		 r7 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b7+=50;
		 bb7=1;
		 if(b7>((screenWidth)))
		 {
			 bb7=0;
		 }
		 bl7++;
	 }
		 }}
	 if(b8+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b8+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r8>(float)(screenHeight/oltay)-1.0)&&(r8<(float)(screenHeight/oltay)+1.0)))
	 {
		t8=1;
		r8=(float)(screenHeight/oltay);
		if(r8>(float)(screenHeight/120))
		 {
			balik8.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t8=0;
		 }
	 }
	 else{ 
		 if(t8==0)
		 { 
	 if(b8>-(screenWidth)&&bb8==0)
	 { 
		if(bk8==1)
		{balik8(1);
		Random rd = new Random();
		 r8 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b8-=60;
		bk8++;
		bl8=1;
	 }
	 else{
		 bk8=1;
		 if(bl8==1)
		 {balik8(2);
		 Random rd = new Random();
		 r8 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b8+=60;
		 bb8=1;
		 if(b8>((screenWidth)))
		 {
			 bb8=0;
		 }
		 bl8++;
	 }
		 }}
	 if(b9+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b9+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r9>(float)(screenHeight/oltay)-1.0)&&(r9<(float)(screenHeight/oltay)+1.0)))
	 {
		t9=1;
		r9=(float)(screenHeight/oltay);
		if(r9>(float)(screenHeight/120))
		 {
			balik9.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t9=0;
		 }
	 }
	 else{ 
		 if(t9==0)
		 { 
	 if(b9>-(screenWidth)&&bb9==0)
	 { 
		if(bk9==1)
		{balik9(1);
		Random rd = new Random();
		 r9 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b9-=15;
		bk9++;
		bl9=1;
	 }
	 else{
		 bk9=1;
		 if(bl9==1)
		 {balik9(2);
		 Random rd = new Random();
		 r9 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b9+=15;
		 bb9=1;
		 if(b9>((screenWidth)))
		 {
			 bb9=0;
		 }
		 bl9++;
	 }
		 }}
	 if(b10+(int)(screenWidth/3.40)>(float)(screenWidth/1.20)&&b10+(int)(screenWidth/3.40)<(float)(screenWidth/1.15)&&((r10>(float)(screenHeight/oltay)-1.0)&&(r10<(float)(screenHeight/oltay)+1.0)))
	 {
		t10=1;
		r10=(float)(screenHeight/oltay);
		if(r10>(float)(screenHeight/120))
		 {
			balik10.eraseColor(android.graphics.Color.TRANSPARENT);
			tuttu++;
			t10=0;
		 }
	 }
	 else{ 
		 if(t10==0)
		 { 
	 if(b10>-(screenWidth)&&bb10==0)
	 { 
		if(bk10==1)
		{balik10(1);
		Random rd = new Random();
		 r10 = rd.nextFloat()*(float)2.10+(float)(1.30);
		}
		b10-=18;
		bk10++;
		bl10=1;
	 }
	 else{
		 bk10=1;
		 if(bl10==1)
		 {balik10(2);
		 Random rd = new Random();
		 r10 = rd.nextFloat()*(float)2.10+(float)(1.30);
		 }
		 b10+=18;
		 bb10=1;
		 if(b10>((screenWidth)))
		 {
			 bb10=0;
		 }
		 bl10++;
	 }
		 }
	if(mause==false)
	{
		oltay+=15;
		if(oltay>(screenHeight/1.05))
		 {	
        intent.putExtra("score",tuttu);
        intt++;
        if(intt==1)
		startActivity(intent);
		 }
	}
	 
	 }
 }
 public void kopekbalik(int i)
 {
	 if(i==1)
	 {
			kbalik =BitmapFactory.decodeResource(getResources(),R.drawable.kbalikleft);
			kbalik = Bitmap.createScaledBitmap(kbalik,(int)(screenWidth/3.40),(int)(screenHeight/4.50), true);
			}
	 if(i==2)
	 {
		 kbalik =BitmapFactory.decodeResource(getResources(),R.drawable.kbalikright);
		 kbalik = Bitmap.createScaledBitmap(kbalik,(int)(screenWidth/3.40),(int)(screenHeight/4.50), true);
			}
		
 }
 public void balik1(int i)
{
	 if(i==1)
	 {
		 balik1 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
		 balik1 = Bitmap.createScaledBitmap(balik1,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		 balik1 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik1 = Bitmap.createScaledBitmap(balik1,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik2(int i)
{
	 if(i==1)
	 {
			balik2 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik2 = Bitmap.createScaledBitmap(balik2,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik2 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik2 = Bitmap.createScaledBitmap(balik2,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik3(int i)
{
	 if(i==1)
	 {
			balik3 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik3 = Bitmap.createScaledBitmap(balik3,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik3 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik3 = Bitmap.createScaledBitmap(balik3,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik4(int i)
{
	 if(i==1)
	 {
			balik4 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik4 = Bitmap.createScaledBitmap(balik4,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik4 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik4 = Bitmap.createScaledBitmap(balik4,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik5(int i)
{
	 if(i==1)
	 {
			balik5 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik5 = Bitmap.createScaledBitmap(balik5,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik5 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik5 = Bitmap.createScaledBitmap(balik5,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik6(int i)
{
	 if(i==1)
	 {
			balik6 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik6 = Bitmap.createScaledBitmap(balik6,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik6 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik6 = Bitmap.createScaledBitmap(balik6,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik7(int i)
{
	 if(i==1)
	 {
			balik7 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik7 = Bitmap.createScaledBitmap(balik7,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik7 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik7 = Bitmap.createScaledBitmap(balik7,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik8(int i)
{
	 if(i==1)
	 {
			balik8 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik8 = Bitmap.createScaledBitmap(balik8,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik8 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik8 = Bitmap.createScaledBitmap(balik8,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik9(int i)
{
	 if(i==1)
	 {
			balik9 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik9 = Bitmap.createScaledBitmap(balik9,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik9 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik9 = Bitmap.createScaledBitmap(balik9,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
 public void balik10(int i)
{
	 if(i==1)
	 {
			balik10 =BitmapFactory.decodeResource(getResources(),R.drawable.balikleft);
			balik10 = Bitmap.createScaledBitmap(balik10,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
	 if(i==2)
	 {
		    balik10 =BitmapFactory.decodeResource(getResources(),R.drawable.balikright);
			balik10 = Bitmap.createScaledBitmap(balik10,(int)(screenWidth/9.40),(int)(screenHeight/10.50), true);
	 }
}
@Override
public boolean onTouchEvent(MotionEvent event) {
	switch (event.getAction() & MotionEvent.ACTION_MASK) {
    case MotionEvent.ACTION_DOWN:
    {
    	dokundu=1;
		break;
	}
    case MotionEvent.ACTION_UP:
    {
    	 
    	dokundu=0;
    	break;
    }
	}
	return true;
}
}
}